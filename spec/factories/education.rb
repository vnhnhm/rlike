FactoryGirl.define do
  factory :application do
    name 'First Application'
  end

  factory :education do
    name 'First Education'
    application_id 1
  end
end
