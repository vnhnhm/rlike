# README

```bash
$ rails db:create
$ rails db:migrate
$ rails db:seed
$ rails server
```

Go to [http://localhost:3000](http://localhost:3000)

See:

`app/controllers/posts_controller.rb`
`app/views/posts/index.html.erb`